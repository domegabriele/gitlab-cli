Command line tool to create gitlab resources (issues and merge requests for now) using the gitlab API.

#### Build

`cargo build`

#### Config

Specify where's your config with the GITLAB_CONFIG_PATH variable:

`export GITLAB_CONFIG_PATH=/path/to/config`

Example of config file:

```json
{
  "api_token": "your_gitlab_api_token",
  "projects": {
    "example": {
      "id": "10976343",
      "name": "example",
      "base_url": "https://gitlab.com"
    },
    "gitlab-cli": {
      "id": "16069457",
      "name": "gitlab-cli",
      "base_url": "https://gitlab.com"
    }
  }
}

```

#### Usage

`$ gitlab --project=PROJECT_NAME issue new`

See `$ gitlab COMMAND help` for more.

Vim is the only supported editor at the moment.
