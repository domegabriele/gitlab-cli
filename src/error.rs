#[derive(Debug)]
pub enum GitlabCliError {
    JsonError(serde_json::error::Error),
    IoError(std::io::Error),
}

impl std::convert::From<serde_json::error::Error> for GitlabCliError {
    fn from(e: serde_json::error::Error) -> Self {
        GitlabCliError::JsonError(e)
    }
}

impl std::convert::From<std::io::Error> for GitlabCliError {
    fn from(e: std::io::Error) -> Self {
        GitlabCliError::IoError(e)
    }
}
