use crate::config::Project;
use reqwest::blocking::Request;
use reqwest::Method;
use reqwest::Url;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::iter::repeat_with;

#[derive(Serialize, Deserialize)]
pub struct Issue {
    pub project: Project,
    pub id: Option<i32>,
    pub title: String,
    pub description: String,
    pub status: Option<bool>,
}

impl Issue {
    pub fn create(
        &self,
        apitoken: &str,
        params: Option<&Vec<(String, String)>>,
    ) -> Result<Request, Box<dyn std::error::Error>> {
        let url = Url::parse_with_params(
            format!(
                "{}/{}/{}",
                self.project.api_url(),
                self.project.id,
                "/issues"
            )
            .as_str(),
            &[
                ("title", self.title.clone()),
                ("description", self.description.clone()),
                ("private_token", apitoken.to_string()),
            ],
        )?;

        let req = Request::new(Method::POST, url);
        Ok(req)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Project;
    use reqwest::get;

    #[test]
    fn test_create_new_issue() {
        let newIssue = Issue {
            project: Project {
                id: "10976343".to_string(),
                name: "test".to_string(),
                base_url: "https://gitlab.com/api/v4/projects/".to_string(),
            },
            id: None,
            title: "test title 2".to_string(),
            description: "test description 2".to_string(),
            status: None,
        };

        let apiToken = "S1uZeB4N_z_MEYLPxh7c";
        let request = newIssue.create(apiToken, None).unwrap();
        let client = reqwest::blocking::Client::new();
        let mut resp = client.execute(request).unwrap();
        println!("{:#?}", resp.text());
    }
}
