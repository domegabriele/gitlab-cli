use crate::config::Project;
use reqwest::blocking::Request;
use reqwest::Method;
use reqwest::Url;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::iter::repeat_with;
use std::process::Command;

pub enum GitBranch {
    Current,
    Master,
    Other(String),
}

impl GitBranch {
    pub fn branch_name(&self) -> Result<String, Box<dyn std::error::Error>> {
        match self {
            GitBranch::Current => {
                let branch = String::new();
                let out = Command::new("/usr/bin/git")
                    .arg("rev-parse")
                    .arg("--abbrev-ref")
                    .arg("HEAD")
                    .output()?;
                let out_str = String::from_utf8(out.stdout)?;
                // remove newline
                let v: Vec<&str> = out_str.split("\n").collect();
                Ok(v[0].to_string())
            }
            GitBranch::Other(s) => Ok(s.to_string()),
            GitBranch::Master => Ok("master".to_string()),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MergeRequest {
    pub project: Project,
    pub id: Option<i32>,
    pub source_branch: String,
    pub target_branch: String,
    pub title: String,
    pub description: String,
    pub status: Option<bool>,
}

impl MergeRequest {
    pub fn new(
        project: Project,
        source: GitBranch,
        target: GitBranch,
        title: String,
        description: String,
    ) -> Result<MergeRequest, Box<dyn std::error::Error>> {
        let mr = MergeRequest {
            project: project,
            id: None,
            source_branch: match source {
                GitBranch::Master => {
                    panic!("Master much be a target branch. Use Current or Other as source")
                }
                b => b.branch_name()?,
            },
            target_branch: target.branch_name()?,
            title: title,
            description: description,
            status: None,
        };
        Ok(mr)
    }
    pub fn create(
        &self,
        apitoken: &str,
        params: Option<&Vec<(String, String)>>,
    ) -> Result<Request, Box<dyn std::error::Error>> {
        let url = Url::parse_with_params(
            format!(
                "{}/{}/{}",
                self.project.api_url(),
                self.project.id,
                "/merge_requests"
            )
            .as_str(),
            &[
                ("title", self.title.clone()),
                ("description", self.description.clone()),
                ("source_branch", self.source_branch.clone()),
                ("target_branch", self.target_branch.clone()),
                ("private_token", apitoken.to_string()),
            ],
        )?;

        let req = Request::new(Method::POST, url);
        Ok(req)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_git_branch() {
        let branch = GitBranch::Current;
        let mut branch_name = branch.branch_name().unwrap();
        println!("{}", branch_name);
        let v: Vec<&str> = branch_name.split("\n").collect();
        println!("{:?}", v);
    }
}
