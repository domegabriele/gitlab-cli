pub mod config;
pub mod editor;
pub mod error;
pub mod resources;

//#[macro_use]
//extern crate tempfile;

use clap::{App, Arg, SubCommand};
use config::{gitlabClient, Config, Project};
use editor::get_text;
use reqwest::blocking::{Client, ClientBuilder};
use reqwest::{Certificate, Identity};
use resources::issue::Issue;
use resources::merge_request::{GitBranch, MergeRequest};
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;
use std::process::exit;

fn main() {
    let matches = App::new("gitlab-cli")
        .version("0.1.0")
        .author("Domenico Gabriele <domenico@blockstream.com>")
        .about("Command line tool using the gitlab api")
        .arg(
            Arg::with_name("project")
                //.short('p')
                .long("project")
                .value_name("PROJECT_NAME")
                .help("Specifies the gitlab project")
                .required(true)
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("issue")
                .about("Issue related commands. See gitlab issue --help for more")
                .subcommand(
                    SubCommand::with_name("new")
                        .about("Creates a new issue for the project specified with --project=PROJECT_NAME")
                )
        )
        .subcommand(
            SubCommand::with_name("merge-request")
                .about("Merge requests related commands. See gitlab merge-request --help for more")
                .subcommand(
                    SubCommand::with_name("new")
                        .about("Creates a new merge request for the project specified with --project=PROJECT_NAME\n\
                        Currently targets master branch and takes current branch as source")
                )
        )
        .get_matches();

    let config = Config::load_config().unwrap();

    let p = match matches.value_of("project") {
        Some(s) => s,
        None => {
            println!("Missing --project argument. Read help for usage");
            exit(-1);
        }
    };

    let api_token = config.api_token;

    let gitlab_client = match config.cert_path {
        Some(cert) => {
            gitlabClient::new_with_identity(api_token, cert, config.cert_password).unwrap()
        }
        None => gitlabClient::new(api_token).unwrap(),
    };

    let mut project = config.projects.get(p).unwrap();
    let p = Project::new(
        project.id.clone(),
        project.name.clone(),
        project.base_url.clone(),
    );

    let api_token = gitlab_client.privateToken.as_str();

    match matches.subcommand() {
        ("issue", Some(issue_matches)) => {
            // Now we have a reference to issue's matches
            if let Some(i) = issue_matches.subcommand_matches("new") {
                let new_issue = Issue {
                    project: p,
                    id: None,
                    title: editor::get_text("issue", "title").unwrap(),
                    description: editor::get_text("issue", "description").unwrap(),
                    status: None,
                };

                let req = new_issue.create(api_token, None).unwrap();
                let mut response = gitlab_client.client.execute(req).unwrap();
                let resp_str = response.text().unwrap();
                println!("{}", resp_str);
            }
        }
        ("merge-request", Some(mr_matches)) => {
            if let Some(i) = mr_matches.subcommand_matches("new") {
                let title = editor::get_text("Merge Request", "title").unwrap();
                let description = editor::get_text("Merge Request", "description").unwrap();
                let new_merge_request =
                    MergeRequest::new(p, GitBranch::Current, GitBranch::Master, title, description)
                        .unwrap();
                println!("{:?}", new_merge_request);
                let req = new_merge_request.create(api_token, None).unwrap();
                let mut response = gitlab_client.client.execute(req).unwrap();
                let resp_str = response.text().unwrap();
                println!("{}", resp_str);
            }
        }
        _ => println!("no match"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_issue_with_cert() {
        let glabClient = gitlabClient::new_with_identity(
            "privatetoken".to_string(),
            "certpath".to_string(),
            Some("certpassword".to_string()),
        )
        .unwrap();

        let testProject = Project {
            id: "839".to_string(),
            name: "test".to_string(),
            base_url: "https://gl.blockstream.io/api/v4/projects/".to_string(),
        };

        let testIssue = Issue {
            project: testProject,
            id: None,
            title: "test issue title".to_string(),
            description: "test issue description ".to_string(),
            status: None,
        };

        let request = testIssue.create(&glabClient.privateToken, None).unwrap();
        glabClient.client.execute(request);
    }
}
