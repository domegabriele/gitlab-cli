use crate::error;
use crate::error::GitlabCliError;
use reqwest::blocking::{Client, ClientBuilder};
use reqwest::{Certificate, Identity};
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::process::exit;

pub struct gitlabClient {
    pub client: Client,
    pub privateToken: String,
}

impl gitlabClient {
    pub fn new(token: String) -> Result<gitlabClient, Box<dyn std::error::Error>> {
        Ok(gitlabClient {
            client: Client::new(),
            privateToken: token.clone(),
        })
    }

    pub fn new_with_identity(
        token: String,
        certificate_path: String,
        certificate_password: Option<String>,
    ) -> Result<gitlabClient, Box<dyn std::error::Error>> {
        let mut c = ClientBuilder::new();
        let mut buf = Vec::new();
        File::open(certificate_path)?.read_to_end(&mut buf)?;

        let identity = match certificate_password {
            Some(pswd) => Identity::from_pkcs12_der(&buf, &pswd)?,
            None => Identity::from_pem(&buf)?,
        };

        let client = c.identity(identity).build()?;

        Ok(gitlabClient {
            client: client,
            privateToken: token,
        })
    }
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub api_token: String,
    pub projects: HashMap<String, Project>,
    pub cert_path: Option<String>,
    pub cert_password: Option<String>,
}

impl Config {
    pub fn load_config() -> Result<Config, GitlabCliError> {
        let mut config_path = match std::env::var("GITLAB_CONFIG_PATH") {
            Ok(path) => path,
            Err(e) => {
                println!("ERROR: env var GITLAB_CONFIG_PATH: {}", e);
                exit(-1);
            }
        };

        let mut conf = File::open(&config_path).unwrap_or_else(|e| {
            if e.kind() == std::io::ErrorKind::NotFound {
                println!("ERROR: Config file not found at {}", config_path);
                exit(-1);
            } else {
                println!("ERROR: Opening {}:\n{}", config_path, e);
                exit(-1);
            }
        });

        let mut config_string = String::new();
        conf.read_to_string(&mut config_string)?;
        let gitlab_config = serde_json::from_str(&config_string)?;

        Ok(gitlab_config)
    }

    pub fn token(&self) -> String {
        self.api_token.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Project {
    pub id: String,
    pub name: String,
    pub base_url: String,
}

impl Project {
    pub fn new(id: String, name: String, base_url: String) -> Project {
        Project { id, name, base_url }
    }

    pub fn api_url(&self) -> String {
        let mut api_url = String::new();
        api_url.push_str(self.base_url.as_str());
        api_url.push_str("/api/v4/projects/");
        api_url
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::{gitlabClient, Config};
    use crate::editor::get_text;
    use crate::resources::issue::Issue;
    use reqwest::get;
    use std::env;

    #[cfg(editor_tests)]
    #[test]
    fn test_new_issue_with_vim() {
        let editor = "EDITOR";
        env::set_var(editor, "vim");

        // Provide issues title and description using vim
        let title = get_text("issue", "title").unwrap();
        let description = get_text("issue", "description").unwrap();

        let new_issue = Issue {
            project: Project {
                id: "10976343".to_string(),
                name: "test".to_string(),
                base_url: "https://gitlab.com/api/v4/projects/".to_string(),
            },
            id: None,
            title,
            description,
            status: None,
        };

        let api_token = "Qx7VxJgwiWjzy2tpEZKD";
        let request = new_issue.create(api_token, None).unwrap();
        let client = reqwest::Client::new();
        let mut resp = client.execute(request).unwrap();

        println!("{:#?}", resp.text());
    }

    #[test]
    fn test_config() {
        env::set_var("GITLAB_CONFIG_PATH", "./config-test");
        /* test config:
         * { "projects" : { "project1" : { "id": "12345", "name" : "project_name", "base_url" : "www.gitlab.com" } } }
         */
        let conf = Config::load_config().unwrap();
        assert_eq!(conf.cert_path, None);
        assert_eq!(conf.projects.get("project1").unwrap().name, "project_name")
    }

    #[test]
    fn test_cert_pem() {
        // TODO: understand why this fails with my cert
        env::set_var("GITLAB_CONFIG_PATH", "./test_config_cert");
        let conf = Config::load_config().unwrap();
        let client = gitlabClient::new_with_identity(
            conf.api_token.clone(),
            "crt.pem".to_string(),
            conf.cert_password,
        )
        .unwrap();
    }
}
