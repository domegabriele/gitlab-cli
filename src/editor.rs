use std::env;
use std::env::temp_dir;
use std::fs::File;
use std::io::{Bytes, Read, Seek, SeekFrom, Write};
use std::path::Path;
use std::process::Command;
static KNOWN_EDITORS: &[&str; 3] = &["/usr/bin/vim", "/usr/bin/vi", "/usr/bin/nano"];

fn get_editor() -> Result<String, Box<dyn std::error::Error>> {
    env::var("EDITOR").or_else(|_| {
        for editor in KNOWN_EDITORS {
            if Path::new(editor).exists() {
                return Ok(editor.to_owned().to_string());
            }
        }

        panic!("Could not find a suitable text editor; Set the EDITOR environment variable")
    })
}

pub fn get_text(text_type: &str, field: &str) -> Result<String, Box<dyn std::error::Error>> {
    let mut path_buf = temp_dir();
    path_buf.push("temp_test");
    let mut temp_path = path_buf.as_path();
    let mut f = File::create(temp_path)?;
    let path = match temp_path.to_str() {
        Some(s) => s,
        None => panic!("Invalid temporary file path: {:?}", f),
    };

    // Write something in the file to help the user
    let header = format!("# Provide {} for the new {}\n\n", field, text_type);
    let bytes = header.as_bytes();
    let written = f.write(bytes)?;
    drop(f);

    let editor = get_editor()?;

    // TODO: +2 says vim to go to line 2.
    // Generalize for nano.
    // Generalize for number of lines in the header
    let status = Command::new(&editor).arg(path).arg("+2").spawn()?.wait()?;

    if !status.success() {
        panic!(
            "Could not launch editor: {} returned {:?}",
            editor,
            status.code()
        )
    }

    let mut f = File::open(temp_path)?;
    let w = written - (1 as usize);
    f.seek(SeekFrom::Start(w as u64))?;

    let mut text = String::new();
    f.read_to_string(&mut text)?;
    drop(f);

    Ok(text)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(editor_tests)]
    #[test]
    fn test_editor() {
        let text = get_text("issue", "title").unwrap();
        assert_eq!(&text, "#issue");
    }

    #[test]
    fn test_temp_files() {
        let mut path_buf = temp_dir();
        path_buf.push("temp_test");

        let mut temp_path = path_buf.as_path();
        let mut f = File::create(temp_path).unwrap();
        let written = f.write("file content".as_bytes()).unwrap();

        // drop file and open again reading to check write was successful
        drop(f);
        let mut f = File::open(temp_path).unwrap();
        let mut content = String::new();
        let red = f.read_to_string(&mut content).unwrap();

        assert_eq!(red, written);
    }
}
